/**
 * Created by scott on 04/02/15.
 */
// http://nodejs.org/api.html#_child_processes
var sys = require('sys')
var exec = require('child_process').exec;
var child;
var passphrase = "passphrase"; //change this
var location = "/Users/scott/.password-store/Email.gpg";

child = exec("gpg --no-tty --passphrase "+passphrase+" --decrypt "+ location, function (error, stdout, stderr) {
    sys.print('stdout: ' + stdout);
    sys.print('stderr: ' + stderr);
    if (error !== null) {
        console.log('exec error: ' + error);
    }
});
