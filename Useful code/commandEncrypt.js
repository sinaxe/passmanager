/**
 * Created by scott on 08/02/15.
 */
// http://nodejs.org/api.html#_child_processes
var sys = require('sys')
var exec = require('child_process').exec;
var child;
var user = "scott.mccullagh@gmail.com"
var location = "< ~/this.txt >"; // '~/' is home location
var destination = "~/encryptedAgain.gpg"

child = exec("gpg -r "+ user+" --encrypt "+ location +" "+destination, function (error, stdout, stderr) {
    sys.print('stdout: ' + stdout);
    sys.print('stderr: ' + stderr);
    if (error !== null) {
        console.log('exec error: ' + error);
    }
});


//gpg -r "scott.mccullagh@gmail.com" --encrypt <this.txt > encrypted.gpg
