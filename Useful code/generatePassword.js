/**
 * Created by scott on 07/03/15.
 */
generatePassword = function(length, sym){
    var letters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var numbers = '0123456789';
    var symbols = '!@£$%^&*()_+=[{]}\|;:/?`~,.<>';
    var all;

    if(sym==false){
        all = letters + numbers;
    }
    else{
        all = letters + numbers + symbols;
    }
    var password ='';
    for(var i=0;i<length;i++){
        var k = (Math.random() * (all.length-1)).toFixed(0);
        password = password + all[k];
    }
    return password
}
var x = generatePassword(12,false);
console.log(x);