PassManager 

Changelog
	
	- 06/04:
		Light code clean up, added PDF of report to repository.
	- 01/04:
		Added Jasmine test classes which test all of the server functionality (all functions detailed in passwordFunctions.js). 
		Minor bug fixes
	- 31/03:
		Clarity added through alerts. Symbols are now not allowed in the name of passwords/notes being added. Input not accepted unless all fields are filled. XSS prevented with special characters being removed from password data.
	- 30/03:
		Checkbox to disbable bitbucket information on settings page to increase clarity
	- 26/03:
		CSS changes	
	- 25/03: 
		Made the main page look better css/etc
		Added the ability to create multi-line passwords - called Notes
		When editing content, the previous content is placed in the box to allow for building on top of the previous content.
	- 24/03:
		Redesigned the main page of the app
			- uses angularJS
			- displays content based on what the user chooses
	- 22/03:
		Fixed a bug where after initialising the app, changing the settings would cause
		everything to break
		Minor error handling, redirect to settings on error
	- 21/03:
		Validation for settings page to make sure input is entered into all fields. Alert shows up on both failure to do this and success of doing this
		More minor updates
	- 19/03:
		Settings page modified to ask for correct data which is then manipulated once submitted - this will be the new index page 
	- 17/03:
		Moved adding and editing out of the main page into seperate pages.
		Index is now the settings page, settings links to index.
		UI overhaul 
	- 08/03:
		Removed hardcoding of /Users/scott/WebApp/ to use path.join and process.cwd() so should work on any system not just mine.
		Can now generate passwords of variable length through the webapp. Probably going to make adding and editing password seperate pages instead of trying to squeeze everything onto a single page.
		Added nav bar fixed to top of the page.
	- 07/03:
		All functions moved out of index.js and into a function only file called passwordFunctions.js - this cleans up the code in index.js.	
		Added code for generating a password
	- 06/03:
		Slight UI redesign; editing section now a single form with 2 buttons depending on what 
		action the user wants to take.
		Editing/deleting password posts form to /passwords/edit then redirects to /passwords
		when its finished doing what it needs to do - this means less code in the post section
		for /passwords.
	- 05/03:
		Code added for editing a password already in the store
		Passwords can now be edited from webapp. Core functionality almost achieved. Just
		generation of passwords to be done. Needs UI redesign
	- 04/03:
		Passwords can be removed from the web app - changes are immediately pushed to the repo
	- 03/03:
		New passwords are now both added locally (within in the project) and pushed to the repo
		given by the user
		Added function to remove password from local repo then push changes to remote, no
		UI elements to deal with this yet though
	- 02/03:
		Web app now clones git repository instead of accessing password store directly
		not entirely sure the effect this will have on interoperability with Pass
	- 01/03:
		Git code using shelljs - working test code added running a scenario of cloning
		then adding a new file and pushing back to remote repository
	- 23/02:
		Temp file deleted after password is encrypted and added to the store
		Master password now asked for on the index page
		Password vault location now asked for on the index page
		GPG ID (email) asked for on the index page - this will probably cause
		issues if more than one GPG key share the same email
	- 22/02:
		Webpage displays password names and relevant password in a table
		Passwords can be added with form on same webpage as table, when
		passwords are added page refreshes and are displayed in the table
	- 19/02:
		Decrypt password + read file name functions now self sufficient
		Functions perform in a synchronous manner
		Code to pull passwords and names through to front end
	- 17/02:
		Messed around with layout options
		Intial redesign of user interface
		Messed around with including bootstrap css
	- 16/02:
		Git repo cleanup - moving files into folders etc
		Added documentation folder
			- draft chapters which were submitted Friday 13th
		Start on front end with Express.
		Pulling password indentifiers through to the display from code running on server side
	- 08/02:
		Encrypt .txt file and save as .gpg file in user home directory
	- 04/02:
		Encrypt a message with openpgp.js library
		Decrypt a message with openpgp.js library
		Currently reads encrpyted armored keys from filesystem - reads message from filesystem too
		^Might not be very useful as keys are stored as .gpg files which I can't seem to get working with the openpgp.js library
		Started work on calling the command line with child processes - see how this pans out
	- 03/02:
		Read local password store (/.password-store)
		Started work on gpg decryption with openpgp.js library


Notes

	https://passmanager.wordpress.com
	This is a blog I've been keeping - it contains more of my day-to-day thoughts, whereas this
	readme will be more for actual progress(?)

	gpg --decrypt --passphrase "passphrase" file 
	Terminal command to decrypt and return password

	gpg -r "userID" --encrypt < sourceFile > destinationFilename
	Terminal command to encrypt a source file and save as destinationFilename