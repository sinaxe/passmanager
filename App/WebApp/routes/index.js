var express = require('express');
var fs  = require("fs");
var router = express.Router();
var passwordFunctions = require("./passwordFunctions.js");
var path = require('path');
var rimraf = require("rimraf");
var shell = require('shelljs');

var userPassword = ""; //now prompted for on the index page
var repo = "";
var userEmail = ""; //user Email for gpg
var passwordStore = path.join(__dirname, '../password-store/');
//console.log(process.cwd());
var passwordNames;
var passwords;


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Settings' });
});

//functions to do stuff
function updatePasswords(){
    passwordNames = passwordFunctions.getPasswordNames(passwordStore);
    passwords = passwordFunctions.getPasswords(passwordStore,userPassword,passwordNames);
}
// This might not be needed anymore? Due to the above - user is always going to enter details first
// However, this allows page to be refreshed without re-entering details i think

router.get('/passwords', function(req, res){
    updatePasswords();
    res.render('passwords',
        {
            title: 'Passwords',
            collection: passwordNames,
            passwordValues: passwords
        })
});

router.get('/passwords/add', function(req, res){
    res.render('addPassword',
        {
            title: 'Add Password'
        })
});


router.post('/passwords/add', function(req,res){
    console.log(req.body);
    if(req.body.nameAdd != null && req.body.password != null) {
        var passwordName = req.body.nameAdd; // new password name
        var password = req.body.password; // new password data

        passwordName = passwordName.trim();
        passwordName = passwordName.replace(' ','_');
        //adds new password to the local password store (within the project)
        // - leaves behind a .txt file -- Moved function call to delete tmp file inside add function
        passwordFunctions.addPassword(passwordStore, userEmail,password, passwordName);

        //git stuff
        var message = 'Added password for ' + passwordName + '.';
        passwordFunctions.pushPasswordStore(passwordStore,message);

    }
    res.redirect('/passwords');
});

router.get('/passwords/edit', function(req,res){
    res.render('editPassword',
        {
            title: 'Edit Password',
            collection: passwordNames,
            passwordValues: passwords
        })
})

router.get('/passwords/settings', function(req,res){
    res.redirect('/');
});


router.post('/passwords/edit', function(req,res){
    var todo = req.body.btnEdit; //this is either 'edit' or 'delete'
    //console.log(req.body);
    var passwordName = req.body.passwordSelect;
    var ptrimd = passwordName.replace(".gpg", ""); //dont need this anymore i think?

    console.log("Password name "+ptrimd);
    if(todo == 'edit'){
        var passwordData = req.body.password;
        passwordFunctions.editPassword(passwordStore,userEmail,ptrimd, passwordData);

    }
    else if(todo == 'delete'){
        passwordFunctions.deletePassword(passwordStore,ptrimd);

    }
    res.redirect('/passwords');


});

router.post('/passwords/settings', function(req,res){
    //console.log(req.body);
    //gpgEmail gpgPassword gitUsername gitPassword repo
    var repos = req.body.repo;
    repos = repos.replace('www.','');
    repos = repos.replace('http://','');
    repos = repos.replace('https://','');
    //console.log(repo);
    repo = 'https://' + req.body.gitUsername + ':' + req.body.gitPassword + '@' + repos;
    console.log(repo);
    //console.log("Changing Password **************");
    userPassword = req.body.gpgPassword;
    userEmail = req.body.gpgEmail;
    //if(fs.existsSync(passwordStore)) {
    //    rimraf.sync(passwordStore);
    //}
    shell.rm('-fr',passwordStore);
    //shell.mkdir('-p', passwordStore);
    passwordFunctions.loadRespository(passwordStore,repo);
    res.redirect('/passwords');
});



// TEST DATA

//loadRespository('https://sinaxe:qsc7ml0sco@bitbucket.org/sinaxe/passmantest.git');
//shell.echo(shell.ls());
//shell.cd('../password-store');
//shell.echo(shell.ls());
////fs.writeFileSync("/Users/scott/WebApp/password-store/test.txt", "test!");
//shell.exec('git add .');
//shell.exec('git commit -m "message"');
//shell.exec('git push');
//var x = getPasswordNames();
//console.log(x);
//addPassword("abc123", "scott.mccullagh@gmail.com", "Abc");
//deleteTmpFile(passwordStore+'Abc'+'.txt');
//var t = getPasswordNames();
//var x = getPasswords(t);
//console.log(x);
//deletePassword('Pooper');
//var y = x.getPasswordNames(passwordStore);
//console.log(y);

module.exports = router;
