/**
 * Created by scott on 16/03/15.
 */
//Edit password
//Set a hidden field with the new password value
function changePassword(){
    var newPassword = prompt("Enter a new password");
    if(newPassword.length>0) {
        document.getElementById("editPassword").value = newPassword;

        var e = document.getElementById('passwordList');
        var a = e.options[e.selectedIndex].text;
        document.getElementById('editName').value = a;
        return true;
    }
    return false;
}
//Edit password
//Get name from dropdown
function changeName() {
    var e = document.getElementById('passwordList');
    var a = e.options[e.selectedIndex].text;
    document.getElementById('editName').value = a;
}
//Generate password
//Show current value of slider
function showValue(newValue) {
    document.getElementById("generateLength").value = newValue;
}
//Generate password
//Place in password inputbox
function generatePassword(elementID) {
    var letters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var numbers = '0123456789';
    var symbols = '!@£$%^&*()_+=[{]}\|;:/?`~,.<>';
    var all;
    var length = document.getElementById('generateLength').value;
    var sym = true;
    var checkbox = document.querySelector('input[type="checkbox"]');

    sym = checkbox.checked;
    if (sym == false) {
        all = letters + numbers;
    }
    else {
        all = letters + numbers + symbols;
    }
    var password = '';
    for (var i = 0; i < length; i++) {
        var k = (Math.random() * (all.length - 1)).toFixed(0);
        password = password + all[k];
    }
    document.getElementById(elementID).value = password;
}

function validateSettings(){
    var gpgE = document.getElementById('gpgEmail').value;
    var gpgP = document.getElementById('gpgPassword').value;
    var gitU = document.getElementById('gitUsername').value;
    var gitP = document.getElementById('gitPassword').value;
    var repo = document.getElementById('repo').value;
    //console.log('GPG Email: '+gpgE);
    if(gpgE == '' || gpgP =='' || gitU =='' ||
        gitP==''|| repo ==null){
        $(".alert-danger").show();
        return false;
    }
    else{
        $(".alert-danger").hide();
        $(".alert-success").show();
    }
}
