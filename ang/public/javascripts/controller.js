/**
 * Created by scott on 24/03/15.
 */
angular.module('passwordApp', [])
    .controller('UserCtrl', ['$scope', function($scope) {
        $scope.passwords = passwords;
        var passwordNames = [];
        var passwordValues = [];
        $scope.passwords.forEach(function (pass){
            passwordNames.push(pass.name);
            passwordValues.push(pass.password);
        });
        $scope.actualPasswords = [];

        $scope.displayArticle = function (pass) {

            angular.forEach($scope.actualPasswords, function (todo) {
                todo.selected = false;
            });
            pass.selected = true;
        };

        $scope.updateTodos = function () {
            for (var i = 0; i < passwords.length; i++) {
                $scope.actualPasswords[i] = {
                    name: passwordNames[i],
                    password: passwordValues[i],
                    selected: false
                }
            }
        };

    }]);
