/**
 * Created by scott on 16/03/15.
 */
//Edit password
//Set a hidden field with the new password value
function changePassword(){
    var newPassword = prompt("Enter a new password");
    if(newPassword.length>0) {
        document.getElementById("editPassword").value = newPassword;

        var e = document.getElementById('passwordList');
        var a = e.options[e.selectedIndex].text;
        document.getElementById('editName').value = a;
        return true;
    }
    return false;
}
//Edit password
//Get name from dropdown
function changeName() {
    var e = document.getElementById('passwordList');
    var a = e.options[e.selectedIndex].text;
    document.getElementById('editName').value = a;
}
//Generate password
//Show current value of slider
function showValue(newValue) {
    document.getElementById("generateLength").innerHTML = newValue;
}
//Generate password
//Place in password inputbox
function generatePassword(elementID) {
    var letters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var numbers = '0123456789';
    var symbols = '!@£$%^&*()_+=[{]}\|;:/?`~,.<>';
    var all;
    var length = document.getElementById('generateLength').innerHTML;
    var sym = true;
    var checkbox = document.querySelector('input[type="checkbox"]');

    sym = checkbox.checked;
    if (sym == false) {
        all = letters + numbers;
    }
    else {
        all = letters + numbers + symbols;
    }
    var password = '';
    for (var i = 0; i < length; i++) {
        var k = (Math.random() * (all.length - 1)).toFixed(0);
        password = password + all[k];
    }
    document.getElementById(elementID).value = password;
}

function generateEdit(elementID){
    var letters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var numbers = '0123456789';
    var symbols = '!@£$%^&*()_+=[{]}\|;:/?~,.';
    var all;
    var length = document.getElementById('generateLength').innerHTML;
    var sym = true;
    var checkbox = document.querySelector('input[type="checkbox"]');

    sym = checkbox.checked;
    if (sym == false) {
        all = letters + numbers;
    }
    else {
        all = letters + numbers + symbols;
    }
    var password = '';
    for (var i = 0; i < length; i++) {
        var k = (Math.random() * (all.length - 1)).toFixed(0);
        password = password + all[k];
    }
    var boxText = document.getElementById(elementID);
    boxText.value = boxText.value  + password + ' ';
}

function validateSettings(){
    var gpgE = document.getElementById('gpgEmail').value;
    var gpgP = document.getElementById('gpgPassword').value;
    var gitU = document.getElementById('gitUsername').value;
    var gitP = document.getElementById('gitPassword').value;
    var repo = document.getElementById('repo').value;
    var cb = document.getElementById('cb').checked;
    //console.log('GPG Email: '+gpgE);
    if(cb){
        if(gpgE == '' || gpgP =='' || gitU =='' ||
            gitP==''|| repo ==null){
            $(".alert-danger").show();
            return false;
        }
        else{
            $(".alert-danger").hide();
            $(".alert-success").show();
        }
    }
    else{
        if(gpgE == '' || gpgP =='' || repo ==null){
            $(".alert-danger").show();
            return false;
        }
        else{
            $(".alert-danger").hide();
            $(".alert-success").show();
        }
    }

}

function displayInBox(p){
    var text = document.getElementById('editPassword');
    var id = 'h' + p;
    text.value = document.getElementById(id).value;
    //text.value = 'Hi';
}

function disableGit(){
    var checkbox = document.getElementById('cb').checked;
    document.getElementById('gitPassword').disabled = !checkbox;
    document.getElementById('gitUsername').disabled = !checkbox;
}

function validatePasswordFields(){
    var name = document.getElementById('nameAdd').value;
    var password = document.getElementById('password').value;

    var okChars = ' abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

    for(var i=0;i<name.length;i++){
        if(okChars.indexOf(name[i]) == -1){
            $(".alert-danger").show();
            $(".alert-success").hide();
            return false;
        }
    }
    if(name == "" || password == ""){
        $(".alert-danger").show();
        $(".alert-success").hide();
        return false;
    }
    else{
        $(".alert-danger").hide();
        $(".alert-success").show();
    }

}
/**
 * This doesnt actually validate anything - the name is in following the trend set by the previous function.
 * This function displays a success box to the user to let them know something is happening.
 */
function validateEdit(){
    if(document.getElementById('passwordList').value == ''){
        $(".alert-success").hide();
        $(".alert-danger").show();
        return false;
    }
    else{
        $(".alert-success").show();
        $(".alert-danger").hide();
    }
}