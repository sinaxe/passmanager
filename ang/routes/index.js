var express = require('express');
var router = express.Router();
var path = require('path');
var shell = require('shelljs');
var passwordFunctions = require('./passwordFunctions.js');

var passwordStore = path.join(__dirname, '../password-store/');
var userEmail ='';
var userPassword = '';
var repo='';

/* Variables */
var passwordNames = [];
var passwordValue = [];
var passwords = [];

//updatePasswords(); //Get some data


/* Functions */
function updatePasswords() {
    passwordNames = passwordFunctions.getPasswordNames(passwordStore);
    passwordValue = passwordFunctions.getPasswords(passwordStore, userPassword, passwordNames);
    passwords = [];
    for(var x=0;x<passwordNames.length;x++){
        passwordNames[x] = passwordNames[x].replace(".gpg","");
    }
    for (var i = 0; i < passwordNames.length; i++) {
        var ob = {
            name: passwordNames[i],
            password: passwordValue[i]
        }
        passwords.push(ob);
    }
}

/*         GET          */
/* GET Settings Page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Settings' });
});
/* GET Main page */
router.get('/main', function(req, res) {
    updatePasswords();
    res.render('main', {
        title: 'Secure Vault',
        passwords: passwords
    })
});
/* GET page to add password */
router.get('/main/add', function(req,res){
    res.render('addPassword',{
        title: 'Add Password'
    })
});
/* GET oage to add note */
router.get('/main/addNote', function(req,res){
   res.render('addNote',{
       title: 'Add Note'
   })
});
/* GET page to edit */
router.get('/main/edit', function(req,res){
   res.render('edit',{
       title: 'Edit Content',
       collection: passwordNames,
       passwordValues: passwordValue
   })
});
/* GET settings page (2) */
router.get('/main/settings', function(req,res){
    res.redirect('/');
})

/*          POST        */
/* Add Content */
router.post('/main/add', function(req,res){
    //console.log(req.body);
    if(req.body.nameAdd != null && req.body.password != null) {
        var passwordName = req.body.nameAdd; // new password name
        var password = req.body.password; // new password data

        passwordName = passwordName.trim();
        password = passwordFunctions.escapeHtml(password);
        //console.log("PASSWORD: "+password);
        while(passwordName.indexOf(' ')>-1){
            passwordName = passwordName.replace(' ','_');
        }

        //adds new password to the local password store (within the project)
        // - leaves behind a .txt file -- Moved function call to delete tmp file inside add function
        passwordFunctions.addPassword(passwordStore, userEmail,password, passwordName);

        //git stuff
        var message = 'Added password for ' + passwordName + '.';
        passwordFunctions.pushPasswordStore(passwordStore,message);

    }
    res.redirect('/main');
});
/* POST change settings */
router.post('/main/settings', function(req,res){
    console.log(req.body);
    //gpgEmail gpgPassword gitUsername gitPassword repo
    var gitUsername = req.body.gitUsername;
    var gitPassword = req.body.gitPassword;

    var repos = req.body.repo;

    if(gitUsername == undefined || gitPassword == undefined){
        repo = repos;
    }
    else{
        repos = repos.replace('www.','');
        repos = repos.replace('http://','');
        repos = repos.replace('https://','');
        repo = 'https://' + gitUsername + ':' + gitPassword + '@' + repos;
    }
    console.log(repo);
    //console.log("Changing Password **************");
    userPassword = req.body.gpgPassword;
    userEmail = req.body.gpgEmail;
    //if(fs.existsSync(passwordStore)) {
    //    rimraf.sync(passwordStore);
    //}
    passwordFunctions.deletePasswordStore(passwordStore);
    //shell.mkdir('-p', passwordStore);
    passwordFunctions.loadRespository(repo);
    res.redirect('/main');
});
/* POST edit content */
router.post('/main/edit', function(req,res){
    var todo = req.body.btnEdit; //this is either 'edit' or 'delete'
    //console.log(req.body);
    var passwordName = req.body.passwordSelect;
    if(passwordName!=undefined){
        var ptrimd = passwordName.replace(".gpg", "");
    }

    console.log("Password name "+ptrimd);
    if(todo == 'edit'){
        var passwordData = req.body.password;
        passwordData = passwordFunctions.escapeHtml(passwordData);
        passwordFunctions.editPassword(passwordStore,userEmail,ptrimd, passwordData);
        var message = 'Edited password for ' + passwordName + '.';
        passwordFunctions.pushPasswordStore(passwordStore, message);

    }
    else if(todo == 'delete'){
        passwordFunctions.deletePassword(passwordStore,ptrimd);
        var message = "Removed password for " + passwordName + ".";
        passwordFunctions.pushPasswordStore(passwordStore, message);

    }
    res.redirect('/main');


});
module.exports = router;
