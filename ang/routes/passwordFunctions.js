/**
 * Created by scott on 07/03/15.
 * All functions used for index.js
 */
var fs = require('fs');
var shell = require('shelljs');
var sys = require('sys')
var exec = require('child_process'),
    child;
var path = require('path');

//path.join(process.cwd(), '../tmp')
//Local Functions
getPassword = function(passwordStore, passphrase, passwordName) {
    var password;
    var location = passwordStore + passwordName;
    child = exec.execSync("gpg --no-tty --passphrase " + passphrase + " --decrypt " + location);
    //console.log("Here: "+child.toString());
    password = child.toString();
    return password;
};

addPassword = function(passwordStore,userEmail, passwordData,passwordName) {
    var prePassword = path.join(process.cwd(), '../tmp.txt');
    var postPassword = passwordStore + passwordName + ".gpg";
    fs.writeFileSync(prePassword, passwordData + "\n");
    var child = exec.execSync("gpg -r " + userEmail + " --encrypt " + "< " + prePassword + " > " + postPassword);
    fs.unlinkSync(path.join(process.cwd(), '../tmp.txt'));
};


pushPasswordStore = function(passwordStore, message) {
    shell.cd(passwordStore);
    shell.exec('git add .');
    shell.exec('git commit -m "' + message + '"');
    shell.exec('git push');
};

//Public functions
exports.getPasswordNames = function(passwordStore) {
        updateRepository(passwordStore);
        var passwordNames = [];
        var data = fs.readdirSync(passwordStore);
        for (i = 0; i < data.length; i++) {
            if (data[i].indexOf(".gpg") > 0) {
                passwordNames[passwordNames.length] = data[i];
            }
        }
        return passwordNames;

    };

exports.loadRespository = function(location) {
    var pStoreLoc = path.join(__dirname, '../password-store');
    try{
        //console.log(pStoreLoc);
        shell.exec('git clone ' + location + ' ' + pStoreLoc);
    }
    catch(e){
        console.log(e);
    }
};



exports.getPasswords = function(passwordStore,userPassword, passwordNames) {
        var passwords = [];
        for (var i = 0; i < passwordNames.length; i++) {
            var password = getPassword(passwordStore,userPassword, passwordNames[i]);
            // console.log(password);
            passwords[passwords.length] = password;
        }
        return passwords;
};


deletePassword = function(passwordStore, passwordName) {
        //git
        shell.cd(passwordStore);
        shell.rm(passwordName+'.gpg');
        shell.exec('git rm ' + passwordName + '.gpg');

};

exports.editPassword = function(passwordStore,userEmail, passwordName, newPassword) {
        //Remove password
        deletePassword(passwordStore, passwordName);
        //Add it as a new password
        addPassword(passwordStore,userEmail,newPassword, passwordName);

};

exports.deletePasswordStore = function(passwordStore){
    shell.cd(passwordStore);
    shell.cd('..');
    shell.rm('-fr',passwordStore);
}


updateRepository = function(passwordStore){
    shell.cd(passwordStore);
    shell.exec('git pull');
}
/**
 *
 * @param text
 * @returns {string}
 * Borrowed from:
 * http://stackoverflow.com/questions/6234773/can-i-escape-html-special-chars-in-javascript
 */
exports.escapeHtml = function(text) {
    return text
        .replace(/&/g, "&amp;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;")
        .replace(/"/g, "&quot;")
        .replace(/'/g, "&#039;");
}

exports.clearPasswordStore = function(passwordStore){
    shell.cd(passwordStore);
    shell.exec("git rm -r '*'");
    pushPasswordStore(passwordStore,'Cleared password store.');

}

//Export local functions - they're both used locally and publically
exports.addPassword = addPassword;
exports.pushPasswordStore = pushPasswordStore;
exports.updateRepository = updateRepository;
exports.getPassword = getPassword;
exports.deletePassword = deletePassword;