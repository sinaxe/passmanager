/**
 * Created by scott on 01/04/15.
 */
var passwordFunctions = require('./passwordFunctions.js');
var path = require('path');
var pStore = path.join(__dirname, '../password-store');
/**
 * Test what happens when given a bogus repository
 *
 * Command line: node testTry.js
 * Output: fatal: repository 'notreallarepo' does not exist
 */
//passwordFunctions.loadRespository('notreallarepo');

var passwordStoreLength = (passwordFunctions.getPasswordNames(pStore));
for(var i=0;i<passwordStoreLength.length;i++){
    console.log(i + ' - ' + passwordStoreLength[i]);
}
//console.log(passwordStoreLength);