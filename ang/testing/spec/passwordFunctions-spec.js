/**
 * Created by scott on 01/04/15.
 */
var passwordFunctions = require('../../routes/passwordFunctions.js');
var fs = require('fs');
var path = require('path');

/* Test Details */
var pStore = path.join(__dirname, '../../password-store/');
var testRepo = 'https://sinace:test1234@bitbucket.org/sinace/testrepository';
var userEmail = 'test@test.com';
var userPassword = 'test1234';

/* NOTE */
/**
 * Tests are performed in an order which leaves the password store in a state
 * which is then used on following tests.
 */
/* TEST FUNCTIONS */
/**
 * Clones directory then makes sure it is there
 */
describe("Clone repository", function(){
    it("should clone remote repository into project", function(){
        if(fs.existsSync(pStore)){
            passwordFunctions.deletePasswordStore(pStore);
        }
        passwordFunctions.loadRespository(testRepo);
        expect(fs.existsSync(pStore)).toBeTruthy();
    })
});

/**
 * Adds password to the store and makes sure its there
 *
 * Note: Notes are added exactly the same way so a new test seems pointless
 *
 * Requirements: repository should have been cloned already
 */
describe("Add password", function(){
    it("should add password to the store", function(){
        console.log("TEST 2");
        var passwordStoreLength = (passwordFunctions.getPasswordNames(pStore)).length;
        passwordFunctions.addPassword(pStore,userEmail,'adnad1421', 'TestPassword1');
        expect(passwordFunctions.getPasswordNames(pStore).length).toEqual(passwordStoreLength+1);
    })
});

/**
 * Gets a specific password and decrypts it
 *
 * Requirements: password store should exist
 */
describe("Get password", function(){
    it("should fetch a password and decrypt it", function(){
        var passwordData = 'BlueGreen02';
        passwordFunctions.addPassword(pStore, userEmail, passwordData, 'TestPassword2');
        var TestPassword2 = passwordFunctions.getPassword(pStore, userPassword, 'TestPassword2.gpg');
        expect(TestPassword2).toEqual(passwordData+'\n'); //Newline character added when creating the password file
    })
});

/**
 * Gets all password names of passwords currently in the store
 *
 * Requirements: password store should exist
 */
describe("Get password names", function(){
    it('should return an array containing all the names of passwords in the store', function(){
        var passwordNames = passwordFunctions.getPasswordNames(pStore);
        expect(passwordNames.length).toEqual(2); //Returns the correct number of entries
        expect(passwordNames[0]).toContain('TestPassword1'); //toContain so no worrying about the extension
        expect(passwordNames[1]).toContain('TestPassword2');
    })
});

/**
 * Gets all passwords and decrypts them
 *
 * Requirements: password store should exist
 */
describe("Get passwords", function(){
    it("should return an array of decrypted passwords", function(){
        var passwordNames = passwordFunctions.getPasswordNames(pStore);
        var passwords = passwordFunctions.getPasswords(pStore, userPassword, passwordNames);

        expect(passwords.length).toEqual(2);
        expect(passwords.length).toEqual(passwordNames.length);
        expect(passwords[0]).toContain('adnad1421');
        expect(passwords[1]).toContain('BlueGreen02');
    })
});

/**
 * Deletes a specific password
 *
 * Requirements: password store should exist
 */
describe("Delete password", function(){
    it("should remove a password from the password store", function(){
        var passwordNames = passwordFunctions.getPasswordNames(pStore);
        var total = passwordNames.length;
        passwordFunctions.deletePassword(pStore, 'TestPassword1');

        //Make sure one is deleted
        expect(passwordFunctions.getPasswordNames(pStore).length).toEqual(total-1);
        //Make sure it was the right one
        var found = false;
        passwordNames = passwordFunctions.getPasswordNames(pStore);
        passwordNames.forEach( function(name){
            if(name=='TestPassword1.gpg'){
                found=true;
            }
        });
        expect(found).toBeFalsy();
    })
});

/**
 * Edits the content of an existing password
 *
 * Requirements: password store should exist
 */
describe("Edit password", function(){
    it("should modify the contents of an existing password", function(){
        passwordFunctions.addPassword(pStore, userEmail, 'toBeModified', 'TestPassword3');
        passwordFunctions.editPassword(pStore, userEmail, 'TestPassword3', 'toBeAdded');

        expect(passwordFunctions.getPassword(pStore, userPassword, 'TestPassword3.gpg'))
            .toEqual('toBeAdded\n');

    })
});

/**
 * Takes in text and cleans it - strips tags and special characters out which could be malicious
 */
describe("Clean user input", function(){
    it("should remove any malicious input, i.e. script tags etc", function() {
        var text = "<script><alert>This is a XSS attack</alert></script>";
        var cleanText = passwordFunctions.escapeHtml(text);
        expect(cleanText).toEqual("&lt;script&gt;&lt;alert&gt;This is a XSS attack&lt;/alert&gt;&lt;/script&gt;");
    })
});

/**
 * Updates remote repository with local changes.
 *
 * Note: this test also tests updating local content when changes are made to the remote
 */
describe("Update remote repo", function(){
    it("should push local changes to remote", function(){
        passwordFunctions.addPassword(pStore, userEmail, 'pass2134', 'TestPassword4');
        passwordFunctions.pushPasswordStore(pStore, 'Updated with changes');
        passwordFunctions.deletePasswordStore(pStore);
        passwordFunctions.loadRespository(testRepo);

        expect(fs.existsSync(pStore+'TestPassword4.gpg')).toBeTruthy();
    })
});

/**
 * Removes all passwords from the password store
 *
 * Note: only really used in regards to testing
 */
describe("Clear password store", function(){
    it("should remove all passwords from store", function(){
        passwordFunctions.clearPasswordStore(pStore);
        expect(passwordFunctions.getPasswordNames(pStore).length).toEqual(0);
    })
});