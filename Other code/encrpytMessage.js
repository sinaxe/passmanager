var openpgp = require('openpgp');
var fs = require('fs');

var key = fs.readFileSync("/Users/scott/public.txt", "utf8");
var publicKey = openpgp.key.readArmored(key);
var message = 'nothing';

function encrypt() {
    openpgp.encryptMessage(publicKey.keys, 'Hello, World!').then(function (pgpMessage) {
        // success
        console.log(pgpMessage);
        message = pgpMessage;
    }).catch(function (error) {
        // failure
    });
}
encrypt(); // this is async, so to get message variable need to wait before trying to print it

setTimeout(function(){
    console.log(message);
},1000);

