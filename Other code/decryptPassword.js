var openpgp = require('openpgp');
var fs = require('fs');

var key = fs.readFileSync('/Users/scott/private.txt', 'utf8');
var privateKey = openpgp.key.readArmored(key).keys[0];
privateKey.decrypt('passphrase'); //passphrase for decrypting
var pgpMessage = fs.readFileSync('/Users/scott/message.txt', 'utf8');;
pgpMessage = openpgp.message.readArmored(pgpMessage);
openpgp.decryptMessage(privateKey, pgpMessage).then(function(plaintext) {
    // success
    console.log(plaintext);
}).catch(function(error) {
    // failure
    console.log("Didn't work");
});